# SPDX-License-Identifier: AGPL-3.0-or-later
# this file is generated automatically by searx/version.py

VERSION_STRING = "2022.09.02-8e9fb0b4"
VERSION_TAG = "2022.09.02-8e9fb0b4"
GIT_URL = "https://github.com/searxng/searxng"
GIT_BRANCH = "master"
